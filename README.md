## Projeto Netshoes

---

## Resposta da pergunta 1:

GET http://localhost:8080/cliente [Traz a lista todos os clientes]

GET http://localhost:8080/cliente/{id} [Realiza busca na lista do cliente por ID]

POST http://localhost:8080/cliente [Cadastra um novo cliente]

PUT http://localhost:8080/cliente/{id} [Realiza atualização do cadastro de um cliente]

DELETE http://localhost:8080/cliente/{id} [Deleta um cliente pelo ID]

Projeto está no diretório Projeto.

---

Resposta da pergunta 2:

Projeto via diretório Projeto.

---

Resposta da pergunta 3:

SELECT COD_PRODUTO, DESC_PRODUTO, SUM (COD_PRODUTO) AS QUANTIDADE FROM ITENS_PEDIDOS GROUP BY QUANTIDADE HAVING SUM(QUANTIDADE) > 1 ORDER BY QUANTIDADE DESC

---

Resposta da pergunta 4:

db.getCollection('produto').find({"descricao": /Alemanha/});

