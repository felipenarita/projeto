package com.client.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.client.models.Users;

public interface UsersRepository extends JpaRepository<Users, String>{

	Users findByNome(String nome);
}
