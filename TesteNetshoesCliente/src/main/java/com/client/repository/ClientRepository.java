package com.client.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.client.models.Evento;

public interface EventoRepository extends JpaRepository<Evento, String>{

	Evento findByCodigo(long codigo);
}
